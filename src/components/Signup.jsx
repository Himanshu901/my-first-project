import React, { useState } from "react";
import { Button, TextField } from "@mui/material";
import "./SignUp.css";

const SignUp = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const [formErrors, setFormErrors] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    const errors = {};
    if (!formData.firstName.trim()) {
      errors.firstName = "First name is required";
    }
    if (!formData.lastName.trim()) {
      errors.lastName = "Last name is required";
    }
    if (!formData.email.trim()) {
      errors.email = "Email is required";
    } else if (!/\S+@\S+\.\S+/.test(formData.email)) {
      errors.email = "Invalid email address";
    }
    if (!formData.password.trim()) {
      errors.password = "Password is required";
    }

    if (Object.keys(errors).length > 0) {
      setFormErrors(errors);
    } else {
      console.log("Form submitted:", formData);
    }
  };

  return (
    <div className="main-container">
      <div className="form-container">
        <div className="text-container">
          <h1>Learn to code by watching others</h1>
          <p>
            See how experienced developers solve problems in real-time. Watching
            tutorials is great, but understanding how developers think is
            invaluable.
          </p>
        </div>
        <div className="input-container">
          <Button variant="contained" className="call-button blue">
            Try it free 7 days
            <span className="grey-text">then $20/mo. thereafter</span>
          </Button>
          <div className="form">
            <TextField
              type="text"
              name="firstName"
              label="First name"
              variant="outlined"
              className="form-input"
              onChange={handleInputChange}
              error={!!formErrors.firstName}
              helperText={formErrors.firstName}
            />
            <TextField
              type="text"
              name="lastName"
              label="Last name"
              variant="outlined"
              className="form-input"
              onChange={handleInputChange}
              error={!!formErrors.lastName}
              helperText={formErrors.lastName}
            />
            <TextField
              type="text"
              name="email"
              label="Email address"
              variant="outlined"
              className="form-input"
              onChange={handleInputChange}
              error={!!formErrors.email}
              helperText={formErrors.email}
            />
            <TextField
              type="password"
              name="password"
              label="Password"
              variant="outlined"
              className="form-input"
              onChange={handleInputChange}
              error={!!formErrors.password}
              helperText={formErrors.password}
            />
            <Button
              variant="contained"
              className="form-button"
              onClick={handleSubmit}
            >
              Claim your free trial
            </Button>
            <p className="form-text">
              By clicking the button, you are agreeing to our{" "}
              <span className="green-text">terms and services</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
